package ua.shpp.builder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.abstract_builder.Director;
import ua.shpp.cars.FordMondeoBuilder;
import ua.shpp.cars.SubaruBuilder;
import ua.shpp.enums.Transmission;

import static ua.shpp.enums.CarEnum.*;

public class AppBuilder {
    static Logger log = LoggerFactory.getLogger(AppBuilder.class);
    public static void main(String[] args) {
        if(args.length==0){
            log.error("Environment variables does not exists");
            System.exit(1);
        }
        Director director = new Director();
        if (returnCarProducer(args[0])==null) {
            log.error("Such producer does not exists");
            System.exit(1);
        }
        if(returnCarProducer(args[0])==FORD_MONDEO){
            director.setBuilder(new FordMondeoBuilder());
        }else if (returnCarProducer(args[0])==SUBARU){
            director.setBuilder(new SubaruBuilder());
        }
        log.info(director.buildCar().toString());
        Car car =new Car.CarBuilder()
                .buildTransmission(Transmission.AUTO)
                .buildMaxSpeed(120)
                .buildProducer("ZAZ").build();
        log.info(car.toString());
        car = new SubaruBuilder().buildMaxSpeed().buildProducer().buildTransmission().build();
        log.info(car.toString());
    }
}