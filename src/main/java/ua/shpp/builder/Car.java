package ua.shpp.builder;

import ua.shpp.enums.Transmission;

import static ua.shpp.enums.Transmission.MANUAL;

public class Car {
    String producer;
    Transmission transmission;
    int maxSpeed;

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public void setTransmission(Transmission transmission) {
        this.transmission = transmission;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString() {
        return "Car{" +
                "producer='" + producer + '\'' +
                ", transmission=" + transmission +
                ", maxSpeed=" + maxSpeed +
                '}';
    }

    public static class CarBuilder extends AppBuilder {
        Car car;

        public CarBuilder() {
            car = new Car();
        }

        public CarBuilder buildProducer(String producer) {
            car.producer = producer;
            return this;
        }

        public CarBuilder buildTransmission(Transmission transmission) {
            car.transmission = transmission;
            return this;
        }

        public CarBuilder buildMaxSpeed(int maxSpeed) {
            car.maxSpeed = maxSpeed;
            return this;
        }

        public Car build() {
            return car;
        }
    }

}
