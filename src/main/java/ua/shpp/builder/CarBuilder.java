//package ua.shpp.builder;
//
//import ua.shpp.enums.Transmission;
//
//import static ua.shpp.enums.Transmission.MANUAL;
//
//public class Builder {
//    private String producer = "ford";
//    private Transmission transmission = MANUAL;
//    private int maxSpeed = 170;
//
//    CarBuilder buildProducer(String producer) {
//        this.producer = producer;
//        return this;
//    }
//
//    CarBuilder buildTransmission(Transmission transmission) {
//        this.transmission = transmission;
//        return this;
//    }
//
//    CarBuilder buildMaxSpeed(int maxSpeed) {
//        this.maxSpeed = maxSpeed;
//        return this;
//    }
//
//    Car build() {
//        Car car = new Car();
//        car.setProducer(this.producer);
//        car.setTransmission(this.transmission);
//        car.setMaxSpeed(this.maxSpeed);
//        return car;
//    }
//}
