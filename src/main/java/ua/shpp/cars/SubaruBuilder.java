package ua.shpp.cars;

import ua.shpp.abstract_builder.AbstractBuilder;

import static ua.shpp.enums.Transmission.MANUAL;

public class SubaruBuilder extends AbstractBuilder{

    @Override
    public AbstractBuilder buildProducer() {
        car.setProducer("SubaruBuilder");
        return this;
    }

    @Override
    public AbstractBuilder buildTransmission() {
        car.setTransmission(MANUAL);
        return this;
    }

    @Override
    public AbstractBuilder buildMaxSpeed() {
        car.setMaxSpeed(250);
        return this;
    }
}
