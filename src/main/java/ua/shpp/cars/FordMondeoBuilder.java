package ua.shpp.cars;

import ua.shpp.abstract_builder.AbstractBuilder;

import static ua.shpp.enums.Transmission.AUTO;

public class FordMondeoBuilder extends AbstractBuilder{

    @Override
    public AbstractBuilder buildProducer() {
        car.setProducer("Ford Mondeo");
        return this;
    }

    @Override
    public AbstractBuilder buildTransmission() {
        car.setTransmission(AUTO);
        return this;
    }

    @Override
    public AbstractBuilder buildMaxSpeed() {
        car.setMaxSpeed(150);
        return this;
    }
}
