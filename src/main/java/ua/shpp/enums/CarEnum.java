package ua.shpp.enums;

import java.util.Arrays;

public enum CarEnum {
    FORD_MONDEO("Mondeo"),
    SUBARU("Subaru");
    String producer;

    CarEnum(String producer) {
        this.producer = producer;
    }
    String getValue(){
        return producer;
    }
    public static CarEnum returnCarProducer(String producer){
        return Arrays.stream(CarEnum.values()).filter(p->p.getValue().equals(producer)).findFirst().orElse(null);
    }
}
