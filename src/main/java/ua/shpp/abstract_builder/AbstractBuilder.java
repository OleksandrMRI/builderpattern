package ua.shpp.abstract_builder;

import ua.shpp.builder.Car;

public abstract class AbstractBuilder {
    protected Car car = new Car();

    public abstract AbstractBuilder buildProducer();

    public abstract AbstractBuilder buildTransmission();

    public abstract AbstractBuilder buildMaxSpeed();

    public Car build() {
        return car;
    }
}
