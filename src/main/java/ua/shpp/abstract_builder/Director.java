package ua.shpp.abstract_builder;

import ua.shpp.builder.Car;

public class Director {
    AbstractBuilder carBuilder;

    public void setBuilder(AbstractBuilder carBuilder) {
        this.carBuilder = carBuilder;
    }

    public Car buildCar() {
        return carBuilder.buildProducer()
                .buildTransmission()
                .buildMaxSpeed()
                .build();
    }
}
